make_poll
==========================

A tool to create polls using python3 and flask

It is an alpha Version, it needs a lot of improvements. Nevertheless it is ready to be used. At the moment it can 
manage text, textarea and radiobuttons fields

Instalation:
==========================

Clone the repository

    git clone git@gitlab.com:mperezfra/make_poll.git

Install the needed python modules.

```bash
virtualenv -p $(which python3) python3venv
source python3venv/bin/activate
pip install flask
pip install pyyaml
pip install wtforms
pip install flask_sqlalchemy
pip install flask_wtf
pip install flask_excel
```

and that's all.

Dependencies
==========================
flask, pyyaml, wtforms, flask_sqlalchemy, flask_wtf, flask_excel

Configuration
==========================

The config.yml.ejemplo shows an example. Copy it to config.yml and modify it as you want.

A SECRET KEY  is generated everytime the app is restarted. The secret key is used to sign the session cookie. Regenerating the key, all the existing sessions would be invalidated. 

If you need a fixed secret key execute the following and paste the code in this code

    python -c 'import os; print(os.urandom(16))'

See http://flask.pocoo.org/docs/1.0/config/

Execution
==========================

execute 

    python make_poll.py

this will create a folder and a .py file based on the configuration on `config.yml` (`f/example.py`, for example, where `f` is the name of the folder parameter and `example` is the name of the filename parameter in `config.yml` file)

to run the poll, execute

    cd f
    python example.py

or

    (cd f; python example.py)

the poll will be accesible in https://server:5000/filename/

where `filename` is the one in the configuration file

the data will be stored in a sqlite3 database in `f/example.db`

