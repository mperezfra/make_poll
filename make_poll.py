#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml

with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


#The names of the attributes begin with the key of the question because when exporting to excel
#The columns are sorted using the name. The firsts ones (id, ipaddr and curdate) are preceded by 'a' and numbers, the rest by 'z' and the key name
questions=cfg['questions']
for question in sorted(questions):
    questions[question]['id']='z'+question+'_'+questions[question]['id']



#for section in cfg:
#    print(section)
#print(cfg['title'])
#print(cfg['subtitle'])
#print(cfg['questions'])
#print(cfg['other'])

import os 

folder=cfg['folder'].replace('-','_')
filename=cfg['filename'].replace('-','_')



basedir = os.path.abspath(os.path.dirname(__file__))



folder = os.path.join(basedir, folder)
dbfolder = os.path.join(folder, 'instance')
absfilename = os.path.join(folder,filename)
databasefilename = filename+'.db'
absdatabasefilename = os.path.join(dbfolder,filename+'.db')



##############################################
# Create the folder and dbfolder
##############################################
if not os.path.exists(folder):
        os.makedirs(folder)
if not os.path.exists(dbfolder):
        os.makedirs(dbfolder)


##############################################
# Create the flask file
##############################################
f = open(absfilename+'.py', 'w')

f.write("""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, render_template, flash, request, redirect, url_for
from flask import Markup
from wtforms import TextField, TextAreaField, validators, StringField, SubmitField

from flask_sqlalchemy import SQLAlchemy

from flask_excel import init_excel, make_response_from_tables

from flask_wtf import FlaskForm

from wtforms import StringField, BooleanField, PasswordField, RadioField, SelectField,SelectMultipleField
from wtforms import TextField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, EqualTo,Email,Optional

from wtforms.widgets import ListWidget, CheckboxInput, TextArea
 
# App config.
#DEBUG = True
app = Flask(__name__,  instance_relative_config=True)
app.config.from_object(__name__)


# A SECRET KEY  is generated everytime the app is restarted.The secret key is used to sign the session cookie. Regenerating the key, all the existing sessions would be invalidated. 
# If you need a fixed secret key execute the following and paste the code in this code
#python -c 'import os; print(os.urandom(16))'
# http://flask.pocoo.org/docs/1.0/config/
import os,binascii
app.config.from_mapping(
        SECRET_KEY = binascii.b2a_hex(os.urandom(15)),
        SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(app.instance_path, '{0}'),
    )

db=SQLAlchemy(app)

init_excel(app)

""".format(databasefilename))

TAB="    "

f.write("""

#The names of the attributes begin with the key of the question because when exporting to excel
#The columns are sorted using the name. The firsts ones (id, ipaddr and curdate) are preceded by 'a' and numbers, the rest by 'z' and the key name
class PollData"""+filename+"""(db.Model):
"""+TAB+"""a010_id = db.Column(db.Integer, primary_key=True)
"""+TAB+"""a020_ipaddr = db.Column(db.Integer)
"""+TAB+"""a030_curdate = db.Column(db.TIMESTAMP)
""")

questions=cfg['questions']
for question in sorted(questions):
    typefield=questions[question]['type']

    if typefield == 'text': 
        f.write(TAB+"""{0} = db.Column({1})\n""".format(questions[question]['id'],'db.String(256)'))
    elif typefield == 'textarea': 
        f.write(TAB+"""{0} = db.Column({1})\n""".format(questions[question]['id'],'db.String(1024)'))
    elif typefield == 'radio': 
        f.write(TAB+"""{0} = db.Column({1})\n""".format(questions[question]['id'],'db.String(64)'))
    else:
        import sys
        f.write("\n\n"+typefield+" Not knownn")
        sys.exit()


f.write('''
# forms model
class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()

class PollForm'''+filename+'''(FlaskForm):
''')


#FIXME: Validators y tipos de campos, para radio por ejemplo
for question in sorted(questions):
    f.write(TAB+questions[question]['id']+" = ")
    typefield=questions[question]['type']
    if typefield == 'text': 
        f.write('TextField("'+questions[question]['title']+'")\n')
    elif typefield == 'textarea': 
        f.write('TextAreaField("'+questions[question]['title']+'", widget=TextArea())\n')
    elif typefield == 'radio': 
        f.write('RadioField("'+questions[question]['title']+'",\n'+TAB+TAB+TAB+"choices=[")
        for l in questions[question]['values']:
            f.write(str(l)+", ")
        f.write("])\n")
        
    else:
        import sys
        f.write("\n\n"+typefield+" Not knownn")
        sys.exit()

f.write("""
    submit = SubmitField("Send")

    def validate(self):
        #print("Validando")
        return True
        validate_form=FlaskForm.validate(self)  # FIXME
        #if self.email.data != "":
#            #pass
#            Email(self.email.data)
#        nmail = PollData.query.filter_by(email=self.email.data).count()
#        if nmail>0:
#            self.email.errors.append('This email is already in use. Please choose another one.')
#            validate_form=False
        return validate_form

""")

f.write("""

def xstr(s):
    return '' if s is None or s == 'None' else str(s)
def isNone(s):
    return True if s is None or s == 'None' else False


@app.route('/"""+filename+"""/done')
def index():
    print()
    return render_template('index.html')

@app.route('/"""+filename+"""/', methods=['GET', 'POST'])
def poll():
    form = PollForm{0}()
    import datetime

    now = datetime.datetime.now()

    if form.validate_on_submit():
        pollAnswer = PollData{0}(\n""".format(filename))

f.write(TAB+TAB+TAB+"a020_ipaddr = request.remote_addr,\n")
f.write(TAB+TAB+TAB+"a030_curdate = now,\n")

for question in sorted(questions):
    f.write(TAB+TAB+TAB+questions[question]['id']+" = ")
    f.write("form."+questions[question]['id']+".data,\n"+TAB+TAB)

f.write("""
                              )
        db.session.add(pollAnswer)
        db.session.commit()
        #print(form.othersop.data)
        flash('''Gracias por participar. Tus respuestas se han guardado.''')\n""")

for question in sorted(questions):
    f.write(TAB+TAB+"message=Markup('''"+questions[question]['title']+": <b>'''"+"+xstr(form."+questions[question]['id']+".data)+'</b>'"+")\n")
    f.write(TAB+TAB+"if not isNone(form."+questions[question]['id']+".data): flash(message)\n")

f.write("""
       #flash('User '+form.name.data+' added')
        #flash (form.othersop.data)
        return redirect(url_for('index'))
        return redirect(request.args.get('next') or url_for('poll'))

    return render_template('poll.html', form=form)

from flask import send_from_directory

@app.route('/"""+filename+"""/download_source', methods=['GET', 'POST'])
def download_source():
    
    
    tarfilename = '"""+filename+""".tgz' 
    tarfiledir = '"""+filename+"""/static/'
    tarfiledirrelative = 'static'

    print("tartar"+tarfiledir+tarfilename)
    import tarfile
    tar = tarfile.open(tarfiledir+tarfilename, "w:gz")
    for name in ['"""+filename+""".py', 'static', 'templates']:
        tar.add('"""+filename+"""/'+name)
    tar.close()
    
    return send_from_directory(tarfiledirrelative, tarfilename, as_attachment=True)


@app.route('/"""+filename+"""/download_data')
def download_data():
 
    return make_response_from_tables(db.session, [PollData"""+filename+""",] , "csv")

 
    poll_data = PollData{0}""".format(filename)+""".query.all()
    print(poll_data)
    for p in poll_data:
        print(p)
        print(p.mejorasformulario)
    #return render_template('show_user.html', user=user)
    return render_template('index.html')

    
if __name__ == "__main__":
    #app.run(debug=True)
    app.run(host='0.0.0.0')
""")



f.close()


##############################################
# Create the db 
##############################################

if os.path.exists(absdatabasefilename):
    print("The sqlite database already exists, it is not created, remove it first if you want a clean one")
else:
    import sys
    sys.path.append(folder)
    import importlib
    print("*************", dbfolder,filename,"**")
    database=importlib.import_module(filename)
    
    database.db.create_all()

    # The  first record stores the name of the questions 
    '''getattr(database, 'PollDataei1020_1718')
    pollAnswer = getattr(database, 'PollDataei1020_1718')()'''

    getattr(database, 'PollData'+filename)
    pollAnswer = getattr(database, 'PollData'+filename)()
    for question in sorted(questions):
        setattr(pollAnswer,questions[question]['id'],questions[question]['title'])
    database.db.session.add(pollAnswer)
    database.db.session.commit()
    

##############################################
# Create the templates
##############################################


templatesfolder = os.path.join(folder, 'templates')

##############################################
# Create the folder
##############################################
if not os.path.exists(templatesfolder):
        os.makedirs(templatesfolder)
        
import shutil
#FIXME
shutil.copy2(basedir+'/templates/'+'index.html', templatesfolder)
shutil.copy2(basedir+'/templates/'+'macros.html', templatesfolder)

##############################################
# Write the layout.html template
##############################################
f = open(templatesfolder+"/layout.html","w")

f.write("""
<!doctype html>
<!-- <html lang={{ locale }}> -->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" media="screen" href ="/static/css/bootstrap.min.css">
       <link rel="stylesheet" href="/static/css/bootstrap-theme.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <title>{% block title %}{% endblock %}</title>


<style>
.jumbotron {
    background-color: #a351aa; 
    color: #ffffff;
}
</style>
</head>

<body>

{#
    <div class=metanav> 
    <a href="{{ url_for('index') }}">Home</a>
    {% if not g.user.is_authenticated %}
     | <a href="{{ url_for('login') }}">log in</a>
    {% else %} 
     | <a href="{{ url_for('user', nickname=g.user.nickname) }}">Your Profile</a>
     | <a href="{{ url_for('logout') }}">log out</a>
     | logged as: {{g.user.nickname}}
    {% endif %}
    
    </div>  
#}

    <div class="jumbotron text-center">
        <h1>"""+cfg['title']+"""</h1>
        <p>"""+cfg['subtitle']+"""</p>
    </div>

  {#
  {% for message in get_flashed_messages() %}
    <div class=flash>{{ message }}</div>
  {% endfor %}
  #}
  
   {% with messages = get_flashed_messages() %}
    {% if messages %}
    <ul>
    {% for message in messages %}
        <li>{{ message }} </li>
    {% endfor %}
    </ul>
    {% endif %}
    {% endwith %}
 
  {% block body %}{% endblock %}

<br />
<br />

<div id="footer" align="center">
    <div class="license">Except where otherwise noted, content on this webpage is licensed under the following license: 
    <bdi><a href="http://creativecommons.org/licenses/by-sa/3.0/" >CC Attribution-Share Alike 3.0 Unported</a></bdi></div>
    <div class="buttons">
        Powered by <a href="https://gitlab.com/mperezfra/make_poll" title="Powered by make_poll" >make_poll
        {# <img src="/???" width="80" height="15" alt="Powered by PHP" /> #} </a> &nbsp;&#47;&nbsp;
        
        <a href="{{ url_for('download_source') }}" title="source code" >source code
        {# <img src="/???" width="80" height="15" alt="source code" /> #} </a> &nbsp;&#47;&nbsp;     
        
        Licences: <a href="http://creativecommons.org/licenses/by-sa/3.0/" rel="license">content
        {# <img src="/????/cc-by-sa.png" alt="CC Attribution-Share Alike 3.0 Unported" /> #} </a>,     
               
        <a href="https://gitlab.com/mperezfra/make_poll/raw/master/LICENSE">code</a> &nbsp;&#47;&nbsp;
        
        <a href="https://validator.w3.org/nu/?doc={{request.base_url}}" title="Valid HTML5" >Valid HTML5? (comming soon ;-)
        {# <img src="???/button-html5.png" width="80" height="15" alt="Valid HTML5" /> #} </a> &nbsp;&#47;&nbsp;

        <a href="//jigsaw.w3.org/css-validator/check/referer?profile=css3" title="Valid CSS" >
        {# <img src="/???/button-css.png" width="80" height="15" alt="Valid CSS" />#}Valid CSS</a>.

        {# <a href="https://fsf???/donate" title="Donate" ><img
            src="/???/button-donate.png" width="80" height="15" alt="Donate" /></a> #}        
    </div>
</div>




</body>
</html>


""")



##############################################
# Write the poll.html template
##############################################
f = open(templatesfolder+"/poll.html","w")

f.write("""
{% extends "layout.html" %}

{% import 'macros.html' as macros %}

{% block title %}"""+cfg['title']+' - '+cfg['subtitle']+"""{% endblock %}

{% block body %}

<div class="container">

  {# <h1>"""+cfg['title']+"""</h1> #}
  """+cfg['Introduction']+"""
  <hr>
  <div class="form-group">
  <form action="{{ url_for('poll') }}" method=post class="form-horizontal" role="form">
    {{ form.csrf }}
    <div class="form-group">
    {{ form.hidden_tag() }}
""")

#print(cfg['questions'])

for question in sorted(questions):
    typefield=questions[question]['type']
    if typefield == 'text': 
        f.write("""{{ form."""+questions[question]['id']+""".label(class="control-label col-sm-4") }}

     <div class="col-sm-6">
        {{ form."""+questions[question]['id']+"""(class="form-control") }}
        {% for error in form."""+questions[question]['id']+""".errors %}
          <span style="color: red;">[{{error}}]</span>
        {% endfor %}
     </div>
     <br />
<br />""")

    elif typefield == 'textarea': 
        f.write(' {{ form.'+questions[question]['id']+'.label }} <br />\n')
        f.write(' {{ form.'+questions[question]['id']+'(cols="80", rows="5") }}\n <br /><br />')

    elif typefield == 'radio': 
    #FIXME: https://stackoverflow.com/questions/7979548/how-to-render-my-textarea-with-wtforms
    # Sustituie table por dl dd ...
        f.write("""{# Radio button como tabla horizontal, etiquetas arriba #}

 {{ form."""+questions[question]['id']+""".label }}
<table align="center">
<tr>
<td width="100"></td>
{% for subfield in form."""+questions[question]['id']+""" %}
    <td width="100">{{ subfield.label }}</td>
{% endfor %}
<td width="100"></td>
</tr>
<tr>
<td width="100">"""+questions[question]['labels'][0]+"""</td>
 {% for subfield in form."""+questions[question]['id']+""" %}
    <td width="100">{{ subfield }}</td>
{% endfor %}
<td width="100">"""+questions[question]['labels'][1]+"""</td>
</tr>
</table>
 <br />
""")
    f.write("""<hr>""")

    

        
f.write("""

    <br />
{{ form.submit }}
  </form>
{% endblock %}



""")
f.close()


##############################################
# Create the static folder
##############################################


staticfolder = os.path.join(folder, 'static')

##############################################
# Create the folder
##############################################
if not os.path.exists(staticfolder):
        os.makedirs(staticfolder)
        
import shutil
#FIXME
if not os.path.exists(staticfolder+'/css'):
    shutil.copytree(basedir+'/static/'+'css', staticfolder+'/css')
else:
    print("The css folder already exists, it is not created, remove it first if you want a clean one")


if not os.path.exists(staticfolder+'/fonts'):
    shutil.copytree(basedir+'/static/'+'fonts', staticfolder+'/fonts')
else:
    print("The fonts folder already exists, it is not created, remove it first if you want a clean one")

if not os.path.exists(staticfolder+'/js'):
    shutil.copytree(basedir+'/static/'+'js', staticfolder+'/js')
else:
    print("The js folder already exists, it is not created, remove it first if you want a clean one")
